import React, { Component } from 'react';
import { Spin, Modal, Button, Layout, Menu, message, Dropdown, Input, Pagination } from 'antd';
import RouteMenu from './RouteMenu';
import { connect } from 'react-redux';
import { createDiffieHellman } from 'crypto';

const { Header, Content, Footer } = Layout;
const menus = ['movies', 'favorite', 'profile'];
const KEY_USER_DATA = 'user_data';

const Search = Input.Search;

const mapStateToProps = state => {
  console.log('mapStateToProps state ', state)
  return {
    isShowDialog: state.isShowDialog,
    itemMovieClick: state.itemMovieDetail
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onDismissDialog: () =>
      dispatch({
        type: 'dismiss_dialog'
      }),
    onItemMovieClick: item =>
      dispatch({
        type: 'click_item',
        payload: item
      })
  }
}

const menuprofile = (
  <Menu>
    <Menu.Item key={menus[2]}>Profile</Menu.Item>
    <Menu.Item>Logout</Menu.Item>
  </Menu>
)

class Main extends Component {
  state = {
    items: [],
    itemMovie: null,
    pathName: menus[0],
    favItems: [],
    email: ''
  };

  onModalClickOk = () => {
    // TODO: handle something click ok
    this.props.onDismissDialog();
  };

  onModalClickCancel = () => {
    this.props.onDismissDialog();
  };

  componentDidMount() {
    const jsonPro = localStorage.getItem(KEY_USER_DATA);
    this.setState({ email: JSON.parse(jsonPro).email });
    const jsonStr = localStorage.getItem('list-fav');
    if (jsonStr) {
      const items = jsonStr && JSON.parse(jsonStr);
      this.setState({ favItems: items });
    }

    const { pathname } = this.props.location;
    var pathName = menus[0];
    if (pathname !== '/') {
      pathName = pathname.replace('/', '');
      if (!menus.includes(pathName)) pathName = menus[0];
    }
    this.setState({ pathName });
    fetch('https://api.giphy.com/v1/gifs/trending?api_key=8DKRjGuu4cW8GQdhQeQbUxAHh6nbm11S&limit=100&rating=G')
      .then(response => response.json())
      .then(gifs => {
        const flattened = gifs.data.map((gif) => { 
          return { title: gif.title, image_url: gif.images.original.url}
        })
        this.setState({ items: flattened })
      });
      // .then(movies => this.setState({ items: movies.data }));
  }

  onMenuClick = e => {
    var path = '/';
    if (e.key !== 'movies') {
      path = `/${e.key}`;
    }
    this.props.history.replace(path);
  };

  onClickFavorite = () => {
    const itemClick = this.props.itemMovieClick;
    const items = this.state.favItems;

    const result = items.find(item => {
      return item.title === itemClick.title;
    });

    if (result) {
      message.error('This item added favorite', 1);
    } else {
      items.push(itemClick);
      localStorage.setItem('gif_fav', JSON.stringify(items));
      message.success('Saved your favorite movie', 1);
      this.onModalClickCancel();
    }
  }

  render() {
    const item = this.props.itemMovieClick;
    // const image = item.images.downsized
    console.log('render item', item)
    return (
      <div>
        {this.state.items.length ? (
          <div style={{ height: '100vh' }}>
            {' '}
            <Layout className="layout" style={{ background: 'white' }}>
              <Header
                style={{
                  padding: '0px',
                  position: 'fixed',
                  zIndex: 1,
                  width: '100%'
                }}
              >
                <Dropdown overlay={menuprofile}>
                  <Menu
                    theme="dark"
                    mode="horizontal"
                    defaultSelectedKeys={[this.state.pathName]}
                    style={{ lineHeight: '64px', float: 'right' }}
                    onClick={e => {
                      this.onMenuClick(e);
                    }}
                  >
                    <Menu.Item key={menus[2]}>{[this.state.email]}</Menu.Item>
                  </Menu>
                </Dropdown>
                <Menu
                  theme="dark"
                  mode="horizontal"
                  defaultSelectedKeys={[this.state.pathName]}
                  style={{ lineHeight: '64px', float: 'right' }}
                  onClick={e => {
                    this.onMenuClick(e);
                  }}
                >
                  <Menu.Item key={menus[0]}>Home</Menu.Item>
                  <Menu.Item key={menus[1]}>Favorite</Menu.Item>
                </Menu>
                <Search
                  placeholder="SEARCH YOUR GIF"
                  mode="horizontal"
                  size='large'
                  style={{ width: '30%', float: 'right',height:'280' }}
                  onSearch={value => console.log(value)}
                  enterButton
                />
              </Header>
              <Content
                style={{
                  padding: '16px',
                  marginTop: 64,
                  minHeight: '600px',
                  justifyContent: 'center',
                  alignItems: 'center',
                  display: 'flex',
                  background: 'black'
                }}
              >
                <RouteMenu items={this.state.items} />
              </Content>
              <Pagination></Pagination>
              <Footer style={{ textAlign: 'center', background: 'white' }}>
                GIF for MEME @ FRONT
              </Footer>
            </Layout>
          </div>
        ) : (
            <Spin size="large" />
          )}
        {item !== null ? (
          <Modal
            width="40%"
            style={{ maxHeight: '70%' }}
            title={item.title}
            visible={this.props.isShowDialog}
            onCancel={this.onModalClickCancel}
            footer={[
              <Button
                key="fav"
                type="primary"
                icon="heart"
                size="large"
                shape="circle"
                onClick={this.onClickFavorite}
              />,
              <Button
                key="clipboard"
                type="primary"
                icon="link"
                size="large"
                shape="circle"
                onClick={this.onClickFavorite}
              />
            ]}
          >
            <img src={item.image_url} style={{ width: '100%' }} />
          </Modal>
        ) : (
            <div />
          )}
      </div>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Main);
