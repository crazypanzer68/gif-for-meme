import firebase from 'firebase/app';
import 'firebase/app';
import 'firebase/database';
import 'firebase/auth';

const config = {
    apiKey: 'AIzaSyA10qj9q8Seyrta8JdfZNmKSRVDUSwLVvo',
    authDomain: 'giphymeme.firebaseapp.com',
    databaseURL: 'https://giphymeme.firebaseio.com',
    projectId: 'giphymeme',
    storageBucket: 'giphymeme.appspot.com',
    messagingSenderId: '53916747220'
};

firebase.initializeApp(config);

const database = firebase.database();
const auth = firebase.auth();
const provider = new firebase.auth.FacebookAuthProvider();

export { database, auth, provider };